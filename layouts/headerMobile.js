import React, {useState} from 'react';
import NavMenu from "./navMenu";

// media
import humburgerImage from './../../img/header/humburger.svg'
import closeImage from './../../img/header/close.svg'
import HeaderLoginComponent from "./headerLoginComponent";
import HeaderUser from "./headerUser";


function HeaderMobile() {
    const [openMenu, toggleMenu] = useState(false);
    const Body = document.querySelector('body');

    const toggleStateMenu = (state) => {
        toggleMenu(state);
        state === true ? Body.classList.add('disabled-scroll') : Body.classList.remove('disabled-scroll')
    }
    return(
        <div className="header__mobile">
            <button className="header__mobile__button" onClick={ () => toggleStateMenu(!openMenu)}>
                {!openMenu &&
                <img src={humburgerImage} alt="humburger"/>
                }
                {openMenu &&
                <img src={closeImage} alt="close"/>
                }
            </button>
            {openMenu &&
            <div className="header__mobile__menu">
                <NavMenu/>
                {typeof Auth === 'undefined' &&
                <HeaderLoginComponent/>
                }
                {typeof Auth !== 'undefined' &&
                <HeaderUser/>
                }
            </div>
            }
        </div>
    )
}

export default HeaderMobile;