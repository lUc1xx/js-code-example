import React, {useRef, useState} from 'react'
import arrow_image from "../../img/header/arrow.svg";
import useOnClickOutside from "../funcs/useOnClickOutside";

function HeaderUser() {
    const [openUserList, toggleUserList] = useState(false);
    const ref = useRef();
    useOnClickOutside(ref, () => toggleUserList(false));

    const sendQueryLogout = () => {
        fetch(Data.link_logout, {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': TOKEN,
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
        })
            .then(res => {
                if (res) {
                    location.reload();
                }
            })
    };

    return (
        <div className="header__user" ref={ref}>
            <button
                className={`header__user__name ${openUserList && 'active'}`}
                onClick={() => toggleUserList(!openUserList)}>
                {Auth.name}
                <svg width="20px" height="20px" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 460.8 460.8" ><g><g><path d="M230.432,0c-65.829,0-119.641,53.812-119.641,119.641s53.812,119.641,119.641,119.641s119.641-53.812,119.641-119.641S296.261,0,230.432,0z"/></g></g><g><g><path d="M435.755,334.89c-3.135-7.837-7.314-15.151-12.016-21.943c-24.033-35.527-61.126-59.037-102.922-64.784c-5.224-0.522-10.971,0.522-15.151,3.657c-21.943,16.196-48.065,24.555-75.233,24.555s-53.29-8.359-75.233-24.555c-4.18-3.135-9.927-4.702-15.151-3.657c-41.796,5.747-79.412,29.257-102.922,64.784c-4.702,6.792-8.882,14.629-12.016,21.943c-1.567,3.135-1.045,6.792,0.522,9.927c4.18,7.314,9.404,14.629,14.106,20.898c7.314,9.927,15.151,18.808,24.033,27.167c7.314,7.314,15.673,14.106,24.033,20.898c41.273,30.825,90.906,47.02,142.106,47.02s100.833-16.196,142.106-47.02c8.359-6.269,16.718-13.584,24.033-20.898c8.359-8.359,16.718-17.241,24.033-27.167c5.224-6.792,9.927-13.584,14.106-20.898C436.8,341.682,437.322,338.024,435.755,334.89z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
            </button>
            <div className={`header__user__list ${openUserList && 'active'}`}>
                <div className="header__user__list__email">{Auth.email}</div>
                <button onClick={sendQueryLogout}>
                    Sign Out
                    <svg id="bold" enable-background="new 0 0 24 24" height="20px" viewBox="0 0 24 24" width="20px" xmlns="http://www.w3.org/2000/svg"><path d="m11 15c-1.654 0-3-1.346-3-3s1.346-3 3-3h5v-6.25c0-1.517-1.233-2.75-2.75-2.75h-10.5c-1.517 0-2.75 1.233-2.75 2.75v18.5c0 1.517 1.233 2.75 2.75 2.75h10.5c1.517 0 2.75-1.233 2.75-2.75v-6.25z"/><path d="m18.617 16.924c-.373-.155-.617-.52-.617-.924v-3h-7c-.552 0-1-.448-1-1s.448-1 1-1h7v-3c0-.404.244-.769.617-.924.374-.155.804-.069 1.09.217l4 4c.391.391.391 1.023 0 1.414l-4 4c-.286.286-.716.372-1.09.217z"/></svg>
                </button>
            </div>
        </div>
    )
}

export default HeaderUser;