import React from 'react';
import HeaderMobile from "./headerMobile";
import NavMenu from "./navMenu";
import HeaderLoginComponent from "./headerLoginComponent";
import HeaderUser from "./headerUser";

function AppBlade() {
    return (
        <div className="header__container container">
            <a href={Data.link_home} className="header__logo">SMS-Rent.com</a>
            <NavMenu/>

            {typeof Auth === 'undefined' &&
            <HeaderLoginComponent/>
            }

            {typeof Auth !== 'undefined' &&
            <HeaderUser/>
            }

            <HeaderMobile/>

        </div>
    )
}

export default AppBlade;