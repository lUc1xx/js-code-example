import React from 'react'

function NavMenu() {
    return (
        <nav className="header__menu">
            <a href="#">Instructions</a>
            <a href="#">FAQ</a>
            <a href="#">Blog</a>
        </nav>
    )
}

export default NavMenu;