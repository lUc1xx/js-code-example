import React from 'react'

// media
import visaImage from './../../img/footer/visa.svg'
import paypalImage from './../../img/footer/paypal.svg'
import fbImage from './../../img/footer/soc-facebook.svg'
import instImage from './../../img/footer/instagram.svg'
import tgImage from './../../img/footer/telegram.svg'
import emailImage from './../../img/footer/emailText.svg'

function Footer() {
    return (
        <div className="container footer__container">
            <div className="footer__soc">
                <a href="#">
                    <img src={fbImage} alt="facebook"/>
                </a>
                <a href="#">
                    <img src={instImage} alt="instagram"/>
                </a>
                <a href="#">
                    <img src={tgImage} alt="telegram"/>
                </a>
            </div>
            <img className="footer__email" src={emailImage}/>
            <div className="footer__payments">
                <img src={visaImage} alt="visa"/>
                <img src={paypalImage} alt="paypal"/>
            </div>
        </div>
    )
}

export default Footer;