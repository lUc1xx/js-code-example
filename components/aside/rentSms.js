import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";

import SelectCountry from "../select/country/selectCountry";
import SelectProvider from "../select/provider/selectProvider";
import searchLoupeImage from "../../../img/aside/search_icon.svg";
import RentServiceItem from "./rentServiceItem";
import Loader from "../extenstions/loader";

import {
    changeCountryActionCreator,
    changeOutputServicesListActionCreator,
    changeProviderActionCreator,
    changeSearchValueActionCreator,
    changeStateShowMoreButtonActionCreator,
    changeStatusActionCreator,
    updateDataThunkCreator,
} from "../../redux/aside/rentSms-reducer";

function RentSms(props) {
    const maxServicesShow = 20;

    const changeCountry = country => {
        props.changeCountry(country);
        props.updateData(country.id, 'any', maxServicesShow);
        props.changeStateShowMoreButton(true);
    };

    const changeProvider = provider => {
        props.changeProvider(provider);
        props.updateData(props.connectedProp.activeCountry.id, provider.code, maxServicesShow, 'changeProvider');
    };

    const onClickMoreButton = () => {
        props.changeOutputServicesList(props.connectedProp.servicesList);
        props.changeStateShowMoreButton(false);
    };

    const changeSearch = (searchValue) => {
        props.changeSearchValue(searchValue);

        let items = [];
        props.connectedProp.servicesList.find(el => {
            let name = el.name.toLowerCase();
            // проверяем совпадает ли значения из поиска с какой то страной
            name.indexOf(searchValue.toLowerCase()) === 0 && items.push(el)
        });

        if (props.connectedProp.stateShowMoreButton === true) {
            items = items.slice(0, maxServicesShow)
        }

        props.changeOutputServicesList(items);
    };

    useEffect(() =>  {
        // выполнений функций при рендере если статус = init
        if (props.connectedProp.status === 'init') {
            props.updateData(props.connectedProp.activeCountry.id, props.connectedProp.activeProvider.code, maxServicesShow);
            props.changeStatus('load');
        }
    }, []);

    return (
        <>
            <div className="aside__text">
                Temp numbers from over 150 countries
            </div>
            <div className="aside__options">
                <SelectCountry
                    data={{
                        activeElement: props.connectedProp.activeCountry,
                        changeCountry: changeCountry,
                        countriesList: props.connectedProp.countriesList,
                    }}/>
                <SelectProvider
                    data={{
                        activeElement: props.connectedProp.activeProvider,
                        changeProvider: changeProvider,
                        providerList: props.connectedProp.providerList
                    }}/>
            </div>
            <div className="aside__search">
                <input
                    type="search"
                    placeholder="Service Search"
                    value={props.connectedProp.searchValue}
                    onChange={e => changeSearch(e.target.value)}
                />
                <button>
                    <img src={searchLoupeImage} alt="loupe"/>
                </button>
            </div>
            <div className="aside__list">

                {props.connectedProp.outputServicesList ?
                    Object.keys(props.connectedProp.outputServicesList).map((key, i) => {
                        return (<RentServiceItem key={key} data={props.connectedProp.outputServicesList[key]}/>)
                    })
                    :
                    <Loader/>
                }

                {props.connectedProp.stateShowMoreButton &&
                <button className="aside__list__more" onClick={onClickMoreButton}>More services</button>
                }
            </div>
            <div className="aside__footer">
                if you Buy a number <br/> you agree with
                <a href="#"> the terms of service</a>
            </div>
        </>
    );
}

const mapStateToProps = state => ({
    connectedProp: state.rentSms
});

const mapDispatchToProps = dispatch => ({
    changeCountry: (value) => {
        dispatch(changeCountryActionCreator(value))
    },
    changeProvider: (value) => {
        dispatch(changeProviderActionCreator(value))
    },
    changeOutputServicesList: (value) => {
        dispatch(changeOutputServicesListActionCreator(value))
    },
    changeStateShowMoreButton: (value) => {
        dispatch(changeStateShowMoreButtonActionCreator(value))
    },
    changeSearchValue: (value) => {
        dispatch(changeSearchValueActionCreator(value))
    },
    changeStatus: (value) => {
        dispatch(changeStatusActionCreator(value))
    },
    updateData: (country_id, provider, maxServicesShow, action) => {
        dispatch(updateDataThunkCreator(country_id, provider, maxServicesShow, action))
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(RentSms);