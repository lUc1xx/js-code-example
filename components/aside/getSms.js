import React, {useEffect} from 'react';
import {connect} from "react-redux";

import SelectCountry from "../select/country/selectCountry";
import SelectProvider from "../select/provider/selectProvider";
import searchLoupeImage from "../../../img/aside/search_icon.svg";
import GetServiceItem from "./getServiceItem";
import Loader from "../extenstions/loader";

import {
    changeCountryActionCreator,
    changeOutputServicesListActionCreator,
    changeProviderActionCreator,
    changeSearchValueActionCreator,
    changeStateShowMoreButtonActionCreator,
    changeStatusActionCreator,
    updateCountriesThunkCreator,
    updateProvidersThunkCreator,
    updateServicesThunkCreator
} from "../../redux/aside/getSms-reducer";

function GetSms(props) {
    const maxServicesShow = 20;

    const changeCountry = country => {
        props.changeCountry(country);
        props.updateProviders(country.id);
        props.updateServices(country.id, maxServicesShow);
        props.changeStateShowMoreButton(true);
    };

    const changeProvider = provider => {
        props.changeProvider(provider);
    };

    const onClickMoreButton = () => {
        props.changeOutputServicesList(props.connectedProp.servicesList);
        props.changeStateShowMoreButton(false);
    };

    const changeSearch = (searchValue) => {
        props.changeSearchValue(searchValue);

        let items = [];
        props.connectedProp.servicesList.find(el => {
            let name = el.name.toLowerCase();
            // проверяем совпадает ли значения из поиска с какой то страной
            name.indexOf(searchValue.toLowerCase()) === 0 && items.push(el)
        });

        if (props.connectedProp.stateShowMoreButton === true) {
            items = items.slice(0, maxServicesShow)
        }

        props.changeOutputServicesList(items);
    };

    useEffect(() =>  {
        // выполнений функций при рендере если статус = init
        if (props.connectedProp.status === 'init') {
            props.updateCountries();
            props.updateProviders(props.connectedProp.activeCountry.id);
            props.updateServices(props.connectedProp.activeCountry.id, maxServicesShow);
            props.changeStatus('load');
        }
    }, []);

    return (
        <>
            <div className="aside__text">
                Temp numbers from over 150 countries
            </div>
            <div className="aside__options">
                <SelectCountry
                    data={{
                        activeElement: props.connectedProp.activeCountry,
                        changeCountry: changeCountry,
                        groupsCountriesList: props.connectedProp.groupsCountriesList,
                        countriesList: props.connectedProp.countriesList,
                    }}/>
                <SelectProvider
                    data={{
                        activeElement: props.connectedProp.activeProvider,
                        changeProvider: changeProvider,
                        providerList: props.connectedProp.providerList
                    }}/>
            </div>
            <div className="aside__search">
                <input
                    type="search"
                    placeholder="Service Search"
                    value={props.connectedProp.searchValue}
                    onChange={e => changeSearch(e.target.value)}
                />
                <button>
                    <img src={searchLoupeImage} alt="loupe"/>
                </button>
            </div>
            <div className="aside__list">

                {props.connectedProp.outputServicesList ?
                    Object.keys(props.connectedProp.outputServicesList).map((key, i) => {
                        return (<GetServiceItem key={key} data={props.connectedProp.outputServicesList[key]}/>)
                    })
                    :
                    <Loader/>
                }

                {props.connectedProp.stateShowMoreButton &&
                <button className="aside__list__more" onClick={onClickMoreButton}>More services</button>
                }
            </div>
            <div className="aside__footer">
                if you Buy a number <br/> you agree with
                <a href="#"> the terms of service</a>
            </div>
        </>
    );
}

const mapStateToProps = state => ({
    connectedProp: state.getSms
});

const mapDispatchToProps = dispatch => ({
    changeCountry: (value) => {
        dispatch(changeCountryActionCreator(value))
    },
    changeProvider: (value) => {
        dispatch(changeProviderActionCreator(value))
    },
    updateCountries: () => {
        dispatch(updateCountriesThunkCreator());
    },
    updateProviders: (country_id) => {
        dispatch(updateProvidersThunkCreator(country_id));
    },
    changeOutputServicesList: (value) => {
        dispatch(changeOutputServicesListActionCreator(value))
    },
    updateServices: (country_id, maxServicesShow) => {
        dispatch(updateServicesThunkCreator(country_id, maxServicesShow))
    },
    changeStateShowMoreButton: (value) => {
        dispatch(changeStateShowMoreButtonActionCreator(value))
    },
    changeSearchValue: (value) => {
        dispatch(changeSearchValueActionCreator(value))
    },
    changeStatus: (value) => {
        dispatch(changeStatusActionCreator(value))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(GetSms);