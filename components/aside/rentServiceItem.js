import React, {useRef, useState} from 'react'

// media
import fbImage from './../../../img/aside/temp/facebook.svg'
import useOnClickOutside from "../../funcs/useOnClickOutside";

function RentServiceItem(props) {
    const [isSelected, selectItem] = useState(false);
    const [numberAmount, changeNumberAmount] = useState(0);
    const [isPhone, resizeWindow] = useState(window.matchMedia('(max-width: 576px)').matches);
    window.addEventListener('resize', () => resizeWindow(window.matchMedia('(max-width: 576px)').matches));

    const styles = {
        background: `linear-gradient(90deg, rgba(${props.data.backgroundColor}, 0) 0%, rgba(${props.data.backgroundColor}) 53.84%, rgba(${props.data.backgroundColor}, 0) 100%)`
    };

    const [openPeriod, changeStatePeriod] = useState(false);
    const [periodValue, changePeriodValue] = useState('hour');
    const periodVars = ['hour', 'day', 'week',];
    const periodRef = useRef();
    useOnClickOutside(periodRef, () => changeStatePeriod(false));

    return (
        <div className="aside__list__service-wrapper">
            <div className="aside__list__service" style={styles} onClick={() => selectItem(!isSelected)}>
                {/*<img alt="image"/>*/}
                <div className="aside__list__service__title">{props.data.name}</div>
                <div className="aside__list__service__amount">{props.data.count} pcs</div>
                {isSelected && isPhone === false &&
                <button className="aside__list__service__buy">Rent</button>
                }
                <div className="aside__list__service__price">{props.data.cost} <span>&#8364;</span></div>
            </div>
            {isSelected &&
            <div className="aside__list__service__options">
                <span>Select rental period</span>
                <div className="aside__list__service__options__period" ref={periodRef} onClick={() => changeStatePeriod(!openPeriod)}>
                    <div className="aside__list__service__options__period-selected">{periodValue}</div>
                    {openPeriod &&
                    <div className="aside__list__service__options__period__list">
                        {periodVars.map(item =>
                            <div
                                key={item}
                                className={periodValue === item ? 'active' : ''}
                                onClick={() => changePeriodValue(item)}
                            >{item}
                            </div>
                        )}
                    </div>
                    }
                </div>
                <input
                    type="number"
                    placeholder="1"
                    value={numberAmount}
                    min={0}
                    max={props.data.count}
                    onChange={e => {
                        changeNumberAmount(e.target.value)
                    }}
                />
                {isSelected && isPhone === true &&
                <button className="aside__list__service__buy">Rent</button>
                }
            </div>
            }
        </div>
    )
}

export default RentServiceItem;