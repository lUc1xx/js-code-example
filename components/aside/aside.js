import React, {useState} from 'react';
import GetSms from "./getSms";
import RentSms from "./rentSms";

function Aside() {
    const [activeTab, changeTab] = useState(1);

    return (
        <>
            <div className="aside__tabs">
                <button className={activeTab === 1 ? 'active' : ''} onClick={() => changeTab(1)}>get a sms</button>
                <button className={activeTab === 2 ? 'active' : ''} onClick={() => changeTab(2)}>rent sms</button>
            </div>
            <div className="aside__content">
                {activeTab === 1 &&
                <GetSms/>
                }
                {activeTab === 2 &&
                <RentSms/>
                }
            </div>

        </>
    )
}

export default Aside;