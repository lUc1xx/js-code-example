import React, {useRef, useState, useEffect} from 'react'
import useOnClickOutside from "../../funcs/useOnClickOutside";

// media
import eyeImage from './../../../img/forms/eye.svg';
import eyeInvisibleImage from './../../../img/forms/eye-invisible.svg';

function InputElement(props) {
    const [labelState, changeLabelState] = useState('initial');
    const ref = useRef(null);
    const changeLabelPosition = () => {
        if (props.value !== undefined) {
            props.value.length < 1 ? changeLabelState('initial') : changeLabelState('top')
        }
    }

    useOnClickOutside(ref, () => changeLabelPosition());

    const [showPasswordType, changeShowPasswordType] = useState('password')
    const showPassword = () => {
        let currentType = ref.current.getAttribute('type'),
            nextType = currentType === 'password' ? 'text' : 'password';

        changeShowPasswordType(nextType)
        ref.current.setAttribute('type', nextType)
    }

    useEffect(() => {
        changeLabelPosition()
    });

    return (
        <div className="form__input-wrapper">
            <div className="form__input">
                <label className={labelState === 'top' ? 'active' : ''}>{props.label}</label>
                <input
                    // props
                    id={props.id}
                    type={props.type}
                    name={props.name}
                    defaultValue={props.value}
                    autoComplete={props.autoComplete}
                    placeholder={props.placeholder}
                    onChange={e => {
                        props.onChange(e.target.value)
                        if (e.target.value.length > 1) {
                            changeLabelState('top')
                        }
                    }}

                    //other
                    onFocus={() => changeLabelState('top')}
                    onKeyUp={() => changeLabelState('top')}
                    ref={ref}
                />
                {props.type === 'password' &&
                <button onClick={showPassword} className="form__input__show-password">
                    {showPasswordType === 'password' ?
                        <img src={eyeInvisibleImage} alt="eyeImage"/>
                        :
                        <img src={eyeImage} alt="eyeImage"/>
                    }
                </button>
                }
            </div>
            {props.validate.valid === false &&
            <div className="form__input__error">
                {props.validate.message}
            </div>
            }
        </div>
    )
}

export default InputElement;