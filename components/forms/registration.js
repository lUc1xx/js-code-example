import React, {useState} from 'react'
import InputElement from "./inputElement";
import {validateEmail, validateName, validatePassword, validateRePassword} from "../../funcs/validate";

function Registration() {
    const [nameValue, changeNameValue] = useState('');
    const [emailValue, changeEmailValue] = useState('');
    const [passwordValue, changePasswordValue] = useState('');
    const [rePasswordValue, changeRePasswordValue] = useState('');

    const [nameValidateData, changeNameValidateData] = useState({
        valid: false,
        message: ''
    });
    const [emailValidateData, changeEmailValidateData] = useState({
        valid: false,
        message: ''
    });
    const [passwordValidateData, changePasswordValidateData] = useState({
        valid: false,
        message: ''
    });
    const [rePasswordValidateData, changeRePasswordValidateData] = useState({
        valid: false,
        message: ''
    });

    const validate = () => {
        changeNameValidateData(validateName(nameValue))
        changeEmailValidateData(validateEmail(emailValue))
        changePasswordValidateData(validatePassword(passwordValue))
        changeRePasswordValidateData(validateRePassword(passwordValue, rePasswordValue))

        if (validateName(nameValue).valid &&
            validateEmail(emailValue).valid &&
            validatePassword(passwordValue).valid &&
            validateRePassword(passwordValue, rePasswordValue).valid)
        {
            sendQuery();
        }
    }

    const sendQuery = () => {
        fetch(Data.link_register, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify({
                _token: TOKEN,
                name: nameValue,
                email: emailValue,
                password: passwordValue,
                password_confirmation: rePasswordValue,
            })
        })
            .then(response =>
                response.json().then(data => ({
                    data: data,
                    status: response.status
                })))
            .then(res => {
                if (res) {
                    if (res.status === 422) { // if error
                        changeEmailValidateData({valid: false, message: res.data.errors.email})
                        changeNameValidateData({valid: false, message: res.data.errors.name})
                        changePasswordValidateData({valid: false, message: res.data.errors.password})
                    }
                    if(res.status === 200) {
                        location.reload();
                    }
                }
            })
    };

    return (
        <div className="form">
            <InputElement
                id="name"
                type="text"
                name="name"
                autoComplete="name"
                value={nameValue}
                validate={nameValidateData}
                onChange={changeNameValue}
                label="Name"
            />
            <InputElement
                id="email"
                type="email"
                name="email"
                autoComplete="email"
                value={emailValue}
                validate={emailValidateData}
                onChange={changeEmailValue}
                label="Email"
            />
            <InputElement
                id="password"
                type="password"
                name="password"
                autoComplete="new-password"
                value={passwordValue}
                validate={passwordValidateData}
                onChange={changePasswordValue}
                label="Password"
            />
            <InputElement
                id="password-confirm"
                type="password"
                name="password_confirmation"
                autoComplete="new-password"
                value={rePasswordValue}
                validate={rePasswordValidateData}
                onChange={changeRePasswordValue}
                label="Confirm Password"
            />
            <button className="form__submit" onClick={validate}>Sign up</button>
            <div className="form__or">- OR -</div>
            <a href={Data.link_login} className="form__link-signin">Sign in</a>
        </div>
    )
}

export default Registration;