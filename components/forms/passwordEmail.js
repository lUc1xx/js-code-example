import React, {useState} from 'react'
import InputElement from "./inputElement";
import {validateEmail} from "../../funcs/validate";

function PasswordEmail() {
    const [emailValue, changeEmailValue] = useState('');
    const [emailValidateData, changeEmailValidateData] = useState({
        valid: false,
        message: ''
    });
    const [formMessage, changeFormMessage] = useState({
        type: 'success',
        message: ''
    });

    const validate = () => {
        changeEmailValidateData(validateEmail(emailValue))
        if (validateEmail(emailValue).valid) {
            sendQuery();
        }
    };

    const sendQuery = () => {
        fetch(Data.link_passwordEmail, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify({
                _token: TOKEN,
                email: emailValue,
            })
        })
            .then(response =>
                response.json().then(data => ({
                    data: data,
                    status: response.status
                })))
            .then(res => {
                if (res) {
                    if (res.status === 422) { // if error
                        changeEmailValidateData({valid: false, message: res.data.errors.email})
                    }
                    if (res.status === 200) {
                        changeFormMessage({
                            type: 'success',
                            message: res.data.message
                        })
                    }
                }
            })
    };

    return (
        <div className="form">
            {!formMessage.message &&
            <>
                <div className="form__title">Reset password</div>
                <InputElement
                    id="email"
                    type="email"
                    name="email"
                    autoComplete="email"
                    value={emailValue}
                    validate={emailValidateData}
                    onChange={changeEmailValue}
                    label="Email"
                />
                <button className="form__submit" onClick={validate}>Send password reset link</button>
            </>
            }
            {formMessage.message &&
            <>
                <div className={`form__message ${formMessage.type === 'success' ? 'success' : 'error'}`}>
                    {formMessage.message}
                </div>
                <a href={Data.link_login} className="form__submit">Sign In</a>
            </>
            }
        </div>
    )
}

export default PasswordEmail;