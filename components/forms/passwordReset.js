import React, {useState} from 'react'
import InputElement from "./inputElement";
import {validateEmail, validatePassword, validateRePassword} from "../../funcs/validate";

function PasswordReset() {
    const [emailValue, changeEmailValue] = useState(PasswordResetData.email ? PasswordResetData.email : '');
    const [passwordValue, changePasswordValue] = useState('');
    const [rePasswordValue, changeRePasswordValue] = useState('');

    const [emailValidateData, changeEmailValidateData] = useState({
        valid: false,
        message: ''
    });
    const [passwordValidateData, changePasswordValidateData] = useState({
        valid: false,
        message: ''
    });
    const [rePasswordValidateData, changeRePasswordValidateData] = useState({
        valid: false,
        message: ''
    });

    const validate = () => {
        changeEmailValidateData(validateEmail(emailValue))
        changePasswordValidateData(validatePassword(passwordValue))
        changeRePasswordValidateData(validateRePassword(passwordValue, rePasswordValue))

        if (validatePassword(passwordValue).valid &&
            validateEmail(emailValue).valid &&
            validateRePassword(passwordValue, rePasswordValue).valid) {
            sendQuery();
        }
    };
    const sendQuery = () => {
        fetch(Data.link_passwordChange, {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': TOKEN,
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify({
                _token: TOKEN,
                token: PasswordResetData.token ? PasswordResetData.token : null,
                email: emailValue,
                password: passwordValue,
                password_confirmation: rePasswordValue,
            })
        })
            .then(response =>
                response.json().then(data => ({
                    data: data,
                    status: response.status
                })))
            .then(res => {
                if (res) {
                    if (res.status === 422) { // if error
                        changeEmailValidateData({valid: false, message: res.data.errors.email})
                        changePasswordValidateData({valid: false, message: res.data.errors.password})
                    }
                    if (res.status === 200) {
                        location = Data.link_home
                    }
                }
            })
    };

    return (
        <div className="form">
            <div className="form__title">Change password</div>
            <InputElement
                id="email"
                type="email"
                name="email"
                autoComplete="email"
                value={emailValue}
                validate={emailValidateData}
                onChange={changeEmailValue}
                label="Email"
            />
            <InputElement
                id="password"
                type="password"
                name="password"
                autoComplete="new-password"
                value={passwordValue}
                validate={passwordValidateData}
                onChange={changePasswordValue}
                label="Password"
            />
            <InputElement
                id="password-confirm"
                type="password"
                name="password_confirmation"
                autoComplete="new-password"
                value={rePasswordValue}
                validate={rePasswordValidateData}
                onChange={changeRePasswordValue}
                label="Confirm Password"
            />
            <button className="form__submit" onClick={validate}>Change password</button>

        </div>
    )
}

export default PasswordReset;