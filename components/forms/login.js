import React, {useState} from 'react'
import InputElement from "./inputElement";
import {validateEmail, validatePassword} from "../../funcs/validate";

function Login() {
    const [emailValue, changeEmailValue] = useState('');
    const [passwordValue, changePasswordValue] = useState('');

    const [emailValidateData, changeEmailValidateData] = useState({
        valid: false,
        message: ''
    });
    const [passwordValidateData, changePasswordValidateData] = useState({
        valid: false,
        message: ''
    });

    const validate = () => {
        changeEmailValidateData(validateEmail(emailValue))
        changePasswordValidateData(validatePassword(passwordValue))

        if (validateEmail(emailValue).valid &&
            validatePassword(passwordValue).valid )
        {
            sendQuery();
        }
    }

    const sendQuery = () => {
        fetch(Data.link_login, {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': TOKEN,
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify({
                _token: TOKEN,
                email: emailValue,
                password: passwordValue,
            })
        })
            .then(response =>
                response.json().then(data => ({
                    data: data,
                    status: response.status
                })))
            .then(res => {
                if (res) {
                    console.log(res)
                    if (res.status === 422) { // if error
                        changeEmailValidateData({valid: false, message: res.data.errors.email})
                        changePasswordValidateData({valid: false, message: res.data.errors.password})
                    }
                    if(res.status === 200) {
                        location.reload();
                    }
                }
            })
    };

    return (
        <div className="form">
            <InputElement
                id="email"
                type="email"
                name="email"
                autoComplete="email"
                value={emailValue}
                validate={emailValidateData}
                onChange={changeEmailValue}
                label="Email"
            />
            <InputElement
                id="password"
                type="password"
                name="password"
                autoComplete="new-password"
                value={passwordValue}
                validate={passwordValidateData}
                onChange={changePasswordValue}
                label="Password"
            />
            <button className="form__submit" onClick={validate}>Sign in</button>
            <div className="form__or">- OR -</div>
            <a href={Data.link_register} className="form__link-signin">Sign up</a>
            <a href={Data.link_reset} className="form__link-reset">Password reset</a>
        </div>
    )
}

export default Login;