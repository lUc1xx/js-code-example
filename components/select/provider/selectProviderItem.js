import React from 'react'

function SelectProviderItem(props) {

    const onClick = () => {
        props.data.changeActiveElement({
            name: props.data.item.name,
            code: props.data.item.operator_code
        })
    };

    return (
        <div
            className={`select__list__item ${props.data.activeElement.code === props.data.item.operator_code ? 'active' : ''}`}
            onClick={onClick}
        >
            {props.data.item.name}
        </div>
    )
}

export default SelectProviderItem;