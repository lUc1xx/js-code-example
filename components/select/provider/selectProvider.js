import React, {useState, useRef} from 'react'
import useOnClickOutside from "../../../funcs/useOnClickOutside";
import SelectProviderItem from "./selectProviderItem";
import SelectCountryGroup from "../country/selectCountryGroup";

function SelectProvider(props) {
    const [isOpenList, toggleList] = useState(false);

    const ref = useRef();
    useOnClickOutside(ref, () => toggleList(false));

    const changeActiveElement = (data) => {
        props.data.changeProvider(data);
        toggleList(false);
    };

    return (
        <div className="select" ref={ref}>
            <div className="select__info" onClick={() => toggleList(!isOpenList)}>
                <div className="select__info__content">{props.data.activeElement.name}</div>
            </div>
            {isOpenList &&
            <div className="select__list">
                {props.data.providerList &&
                <div className="select__list__items">
                    {props.data.providerList.map((item, i) => {
                        return (
                            <SelectProviderItem
                                key={i}
                                data={{
                                    item,
                                    activeElement: props.data.activeElement,
                                    changeActiveElement: changeActiveElement
                                }}
                            />
                        )
                    })}
                </div>
                }
            </div>
            }
        </div>
    )
}

export default SelectProvider;
