import React from 'react'

function SelectCountryItem(props) {

    const onClick = () => {
        props.data.changeActiveElement({
            name: props.data.item.name,
            id: props.data.item.id
        })
    };

    return (
        <div
            className={`select__list__item ${props.data.activeElement.id === props.data.item.id ? 'active' : ''}`}
            onClick={onClick}
        >
            {props.data.item.name}
        </div>
    )
}

export default SelectCountryItem;