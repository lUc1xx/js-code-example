import React, {useState} from 'react'
import SelectCountryItem from "./selectCountryItem";

function SelectCountryGroup(props) {
    const [isOpenList, toggleList] = useState(false);
    return (
        <div
            className="select__list__group"
            onMouseEnter={() => toggleList(true)}
            onMouseLeave={() => toggleList(false)}
            onClick={() => toggleList(!isOpenList)}
        >
            {props.data.name}
            {isOpenList &&
            <div className="select__list__group__list-wrapper">
                <div className="select__list__group__list">
                    {props.data.countries.map((item, i) =>
                        <SelectCountryItem key={i}
                                           data={{
                                               item,
                                               activeElement: props.data.activeElement,
                                               changeActiveElement: props.data.changeActiveElement
                                           }}
                        />
                    )}
                </div>
            </div>
            }
        </div>
    )
}

export default SelectCountryGroup;