import React, {useState, useRef} from 'react'
import useOnClickOutside from "../../../funcs/useOnClickOutside";
import SelectCountryItem from "./selectCountryItem";
import SelectCountryGroup from "./selectCountryGroup";
import {sortObjectCallbackByAlphabet} from "../../../funcs/sorts";


function SelectCountry(props) {
    const [isOpenList, toggleList] = useState(false); // состояние списка
    const [searchValue, changeSearchValue] = useState('');
    const [searchItems, changeSearchItems] = useState(null); // элементы при использовании поиска

    const ref = useRef();
    useOnClickOutside(ref, () => toggleList(false));

    const changeSearch = (searchValue) => {
        changeSearchValue(searchValue);
        let items = [];
        props.data.countriesList.find(el => {
            let name = el.name.toLowerCase();
            // проверяем совпадает ли значения из поиска с какой то страной
            name.indexOf(searchValue.toLowerCase()) === 0 && items.push(el)
        });

        changeSearchItems(sortObjectCallbackByAlphabet(items));
    };

    const changeActiveElement = (data) => {
        props.data.changeCountry(data);
        toggleList(false);
    };

    return (
        <div className="select" ref={ref}>
            <div className="select__info" onClick={() => toggleList(!isOpenList)}>
                <div className="select__info__content">
                    {/*<img src="/images/usa.jpg" alt="alt-test"/>*/}
                    {props.data.activeElement.name}
                </div>
            </div>
            {isOpenList &&
            <div className="select__list">
                <input
                    className="select__list__search"
                    type="text"
                    placeholder="Search by country"
                    value={searchValue}
                    onChange={e => changeSearch(e.target.value)}
                />

                {props.data.groupsCountriesList && searchValue === '' &&
                props.data.groupsCountriesList.map((item, i) => {
                    return (
                        <SelectCountryGroup key={i} data={{
                            name: item.name,
                            countries: item.countries,
                            activeElement: props.data.activeElement,
                            changeActiveElement: changeActiveElement
                        }}/>)
                })
                }

                {props.data.groupsCountriesList === undefined && props.data.countriesList && searchValue === '' &&
                <div className="select__list__items">
                    {props.data.countriesList.map((item, i) =>
                        <SelectCountryItem key={i}
                                           data={{
                                               item,
                                               activeElement: props.data.activeElement,
                                               changeActiveElement: changeActiveElement
                                           }}
                        />)}
                </div>
                }

                {searchItems && searchValue !== '' &&
                <div className="select__list__items">
                    {searchItems.map((item, i) =>
                        <SelectCountryItem key={i} data={{
                            item,
                            activeElement: props.data.activeElement,
                            changeActiveElement: changeActiveElement
                        }}/>)}
                </div>
                }

            </div>
            }
        </div>
    )
}

export default SelectCountry;
