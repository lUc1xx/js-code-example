import {sortObjectCallbackByAlphabet} from "../../funcs/sorts";

const CHANGE_COUNTRY = 'GET_CHANGE_COUNTRY';
const CHANGE_PROVIDER = 'GET_CHANGE_PROVIDER';
const CHANGE_COUNTRIES_LIST = 'GET_CHANGE_COUNTRIES_LIST';
const CHANGE_GROUPS_COUNTRIES_LIST = 'GET_CHANGE_GROUP_COUNTRIES_LIST';
const CHANGE_PROVIDER_LIST = 'GET_CHANGE_PROVIDER_LIST';
const CHANGE_SERVICES_LIST = 'GET_CHANGE_SERVICES_LIST';
const CHANGE_OUTPUT_SERVICES_LIST = 'GET_CHANGE_OUTPUT_SERVICES_LIST';
const CHANGE_STATE_SHOW_MORE_BUTTON = 'GET_CHANGE_STATE_SHOW_MORE_BUTTON';
const CHANGE_SEARCH_VALUE = 'GET_CHANGE_SEARCH_VALUE';
const CHANGE_STATUS = 'GET_CHANGE_STATUS';

let initialState = {
    status: 'init',
    activeCountry: {
        name: 'England',
        id: 16,
    },
    activeProvider: {
        name: 'Any provider',
        code: 'all',
    },
    countriesList: [],
    groupsCountriesList: null,
    providerList: null,
    servicesList: {},
    outputServicesList: null,
    stateShowMoreButton: true,
    searchValue: '',
};

const getSmsReducer = (state = initialState, action) => {

    switch (action.type) {

        case CHANGE_COUNTRY:
            return {
                ...state,
                activeCountry: action.value
            };

        case CHANGE_PROVIDER:
            return {
                ...state,
                activeProvider: action.value
            };

        case CHANGE_COUNTRIES_LIST:
            return {
                ...state,
                countriesList: action.value
            };

        case CHANGE_GROUPS_COUNTRIES_LIST:
            return {
                ...state,
                groupsCountriesList: action.value
            }

        case CHANGE_PROVIDER_LIST:
            return {
                ...state,
                providerList: action.value
            };

        case CHANGE_SERVICES_LIST:
            return {
                ...state,
                servicesList: action.value
            };

        case CHANGE_OUTPUT_SERVICES_LIST:
            return {
                ...state,
                outputServicesList: action.value
            };

        case CHANGE_STATE_SHOW_MORE_BUTTON:
            return {
                ...state,
                stateShowMoreButton: action.value,
            };

        case CHANGE_SEARCH_VALUE:
            return {
                ...state,
                searchValue: action.value
            };

        case CHANGE_STATUS:
            return {
                ...state,
                status: action.value
            };

        default:
            return state;
    }
}

export default getSmsReducer;

export const changeCountryActionCreator = (value) => ({
    type: CHANGE_COUNTRY,
    value: value
});

export const changeProviderActionCreator = (value) => ({
    type: CHANGE_PROVIDER,
    value: value
});

const changeCountriesListActionCreator = (value) => ({
    type: CHANGE_COUNTRIES_LIST,
    value: value
});

const changeGroupsCountriesListActionCreator = (value) => ({
    type: CHANGE_GROUPS_COUNTRIES_LIST,
    value: value
});

const changeProviderListActionCreator = (value) => ({
    type: CHANGE_PROVIDER_LIST,
    value: value
});

const changeServicesListActionCreator = (value) => ({
    type: CHANGE_SERVICES_LIST,
    value: value
});

export const changeOutputServicesListActionCreator = (value) => ({
    type: CHANGE_OUTPUT_SERVICES_LIST,
    value: value
});

export const changeStateShowMoreButtonActionCreator = (value) => ({
    type: CHANGE_STATE_SHOW_MORE_BUTTON,
    value: value
});

export const changeSearchValueActionCreator = (value) => ({
    type: CHANGE_SEARCH_VALUE,
    value: value
});

export const changeStatusActionCreator = (value) => ({
    type: CHANGE_STATUS,
    value: value
});


export const updateCountriesThunkCreator = () => {
    return (dispatch) => {
        fetch(Data.api_getCountries, {
            method: 'GET',
            headers: {
                'token': TOKEN,
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
        })
            .then(response =>
                response.json().then(data => ({
                    data: data,
                    status: response.status
                })))
            .then(res => {
                let groupsCountries = res.data.countries,
                    countries = []; // массив со всеми странами

                // перебираем континенты и страны
                groupsCountries = Object.keys(groupsCountries).map(key => {
                    let groupCountriesItems = []; // массив с странами определенной группы

                    groupsCountries[key].map(item => {
                        groupCountriesItems.push(item);
                        countries.push(item);
                    });

                    return {
                        name: key,
                        countries: sortObjectCallbackByAlphabet(groupCountriesItems)
                    }
                });
                dispatch(changeCountriesListActionCreator(countries));
                dispatch(changeGroupsCountriesListActionCreator(groupsCountries));
            });
    }
};

export const updateProvidersThunkCreator = (country_id) => {
    return (dispatch) => {
        fetch(Data.api_getProviders, {
            method: 'POST',
            headers: {
                'token': TOKEN,
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify({
                country_id: country_id,
            })
        })
            .then(response =>
                response.json().then(data => ({
                    data: data,
                    status: response.status
                })))
            .then(res => {
                if (res.status === 200) {
                    dispatch(changeProviderListActionCreator(res.data.providers));
                    dispatch(changeProviderActionCreator({
                        name: 'Any provider',
                        code: 'all',
                    }));
                }
            });
    }
};

export const updateServicesThunkCreator = (country_id, maxServicesShow) => {
    return (dispatch) => {
        dispatch(changeOutputServicesListActionCreator(null));
        fetch(Data.api_getServices, {
            method: 'POST',
            headers: {
                'token': TOKEN,
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify({
                country_id: country_id,
            })
        })
            .then(response =>
                response.json().then(data => ({
                    data: data,
                    status: response.status
                })))
            .then(res => {
                if (res.status === 200) {
                    dispatch(changeSearchValueActionCreator(''));
                    let services = Object.values(res.data.services);
                    if (services.length <= 30) {
                        dispatch(changeStateShowMoreButtonActionCreator(false))
                    }
                    dispatch(changeOutputServicesListActionCreator(services.slice(0, maxServicesShow)));
                    dispatch(changeServicesListActionCreator(services));
                }
            });
    }
};


