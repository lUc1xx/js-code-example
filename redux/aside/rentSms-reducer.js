const CHANGE_COUNTRY = 'RENT_CHANGE_COUNTRY';
const CHANGE_PROVIDER = 'RENT_CHANGE_PROVIDER';
const CHANGE_COUNTRIES_LIST = 'RENT_CHANGE_COUNTRIES_LIST';
const CHANGE_GROUPS_COUNTRIES_LIST = 'RENT_CHANGE_GROUP_COUNTRIES_LIST';
const CHANGE_PROVIDER_LIST = 'RENT_CHANGE_PROVIDER_LIST';
const CHANGE_SERVICES_LIST = 'RENT_CHANGE_SERVICES_LIST';
const CHANGE_OUTPUT_SERVICES_LIST = 'RENT_CHANGE_OUTPUT_SERVICES_LIST';
const CHANGE_STATE_SHOW_MORE_BUTTON = 'RENT_CHANGE_STATE_SHOW_MORE_BUTTON';
const CHANGE_SEARCH_VALUE = 'RENT_CHANGE_SEARCH_VALUE';
const CHANGE_STATUS = 'RENT_CHANGE_STATUS';

let initialState = {
    status: 'init',
    activeCountry: {
        name: 'England',
        id: 16,
    },
    activeProvider: {
        name: 'Any provider',
        code: 'any',
    },
    countriesList: [],
    groupsCountriesList: null,
    providerList: null,
    servicesList: {},
    outputServicesList: null,
    stateShowMoreButton: true,
    searchValue: '',
};

const rentSmsReducer = (state = initialState, action) => {

    switch (action.type) {

        case CHANGE_COUNTRY:
            return {
                ...state,
                activeCountry: action.value
            };

        case CHANGE_PROVIDER:
            return {
                ...state,
                activeProvider: action.value
            };

        case CHANGE_COUNTRIES_LIST:
            return {
                ...state,
                countriesList: action.value
            };

        case CHANGE_GROUPS_COUNTRIES_LIST:
            return {
                ...state,
                groupsCountriesList: action.value
            }

        case CHANGE_PROVIDER_LIST:
            return {
                ...state,
                providerList: action.value
            };

        case CHANGE_SERVICES_LIST:
            return {
                ...state,
                servicesList: action.value
            };

        case CHANGE_OUTPUT_SERVICES_LIST:
            return {
                ...state,
                outputServicesList: action.value
            };

        case CHANGE_STATE_SHOW_MORE_BUTTON:
            return {
                ...state,
                stateShowMoreButton: action.value,
            };

        case CHANGE_SEARCH_VALUE:
            return {
                ...state,
                searchValue: action.value
            };

        case CHANGE_STATUS:
            return {
                ...state,
                status: action.value
            };

        default:
            return state;
    }
}

export default rentSmsReducer;

export const changeCountryActionCreator = (value) => ({
    type: CHANGE_COUNTRY,
    value: value
});

export const changeProviderActionCreator = (value) => ({
    type: CHANGE_PROVIDER,
    value: value
});

const changeCountriesListActionCreator = (value) => ({
    type: CHANGE_COUNTRIES_LIST,
    value: value
});

const changeProviderListActionCreator = (value) => ({
    type: CHANGE_PROVIDER_LIST,
    value: value
});

const changeServicesListActionCreator = (value) => ({
    type: CHANGE_SERVICES_LIST,
    value: value
});

export const changeOutputServicesListActionCreator = (value) => ({
    type: CHANGE_OUTPUT_SERVICES_LIST,
    value: value
});

export const changeStateShowMoreButtonActionCreator = (value) => ({
    type: CHANGE_STATE_SHOW_MORE_BUTTON,
    value: value
});

export const changeSearchValueActionCreator = (value) => ({
    type: CHANGE_SEARCH_VALUE,
    value: value
});

export const changeStatusActionCreator = (value) => ({
    type: CHANGE_STATUS,
    value: value
});

export const updateDataThunkCreator = (country_id, provider, maxServicesShow, action = null) => {
    return (dispatch) => {
        dispatch(changeOutputServicesListActionCreator(null));
        fetch(Data.api_getRentCountriesAndServices, {
            method: 'POST',
            headers: {
                'token': TOKEN,
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify({
                country_id: country_id,
                provider: provider
            })
        })
            .then(response =>
                response.json().then(data => ({
                    data: data,
                    status: response.status
                })))
            .then(res => {
                if (res.status === 200) {
                    dispatch(changeSearchValueActionCreator(''));
                    dispatch(changeCountriesListActionCreator(res.data.countries));
                    dispatch(changeProviderListActionCreator(res.data.providers));

                    if (action !== 'changeProvider') {
                        dispatch(changeProviderActionCreator({
                            name: 'Any provider',
                            code: 'any',
                        }));
                    }

                    let services = Object.values(res.data.services);
                    if (services.length <= 30) {
                        dispatch(changeStateShowMoreButtonActionCreator(false))
                    }
                    dispatch(changeOutputServicesListActionCreator(services.slice(0, maxServicesShow)));
                    dispatch(changeServicesListActionCreator(services));
                }
            });
    }
};


