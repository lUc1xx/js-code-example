import {combineReducers, createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';

import getSmsReducer from './getSms-reducer.js'
import rentSmsReducer from './rentSms-reducer.js'

let reducers = combineReducers({
    getSms: getSmsReducer,
    rentSms: rentSmsReducer,
});

let asideStore = createStore(reducers, applyMiddleware(thunk));

export default asideStore;