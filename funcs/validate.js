// methods
const beginWithoutDigit = /^\D.*$/;             // не начиналась с цифры
const withoutSpecialChars = /^[^-() /]*$/;      // без спец символов
const containsLetters = /^.*[a-zA-Z]+.*$/;      // проверка на содержания букв
const minimumChars = /^.{8,32}$/;               // от...до символов

const messagesError = new Map([
    ['beginWithoutDigit', 'Must not start with a number. '],
    ['withoutSpecialChars', 'Must not contain special characters. '],
    ['containsLetters', 'Must contain letters. '],
    ['minimumChars', 'Must contain between 8 and 32 characters. ']
]);


// for validate email
export const validateEmail = email => {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let value = String(email).toLowerCase();
    let data = {
        valid: true,
        message: '',
    };

    if (re.test(value) === false) {
        data.valid = false;
        data.message = 'Check your email for correctness. ';
    } else {
        data.valid = true;
        data.message = '';
    }

    return data;
}

// for validate name
export const validateName = name => {

    let data = {
        valid: true,
        message: ''
    };

    const methods = [
        beginWithoutDigit,
        withoutSpecialChars,
        containsLetters
    ];

    const methodsNames = [
        'beginWithoutDigit',
        'withoutSpecialChars',
        'containsLetters'
    ];

    for (let i = 0; i < methods.length; i++) {
        if (methods[i].test(name) === false) {
            data.message += messagesError.get(methodsNames[i])
        }
    }

    for (let item of methods) {
        data.valid = item.test(name)
        if (data.valid === false) {
            break;
        }
    }

    return data
}

// for validate password
export const validatePassword = password => {

    let data = {
        valid: true,
        message: ''
    };

    const methods = [
        minimumChars,
    ];

    const methodsNames = [
        'minimumChars',
    ];

    for (let i = 0; i < methods.length; i++) {
        if (methods[i].test(password) === false) {
            data.message += messagesError.get(methodsNames[i])
        }
    }

    for (let item of methods) {
        data.valid = item.test(password)
        if (data.valid === false) {
            break;
        }
    }

    return data
}

// for validate rePassword
export const validateRePassword = (password, rePassword) => {
    let data = {
        valid: true,
        message: ''
    };

    if (password === rePassword) {
        data.valid = true
        data.message = ''
    } else {
        data.valid = false
        data.message = 'Passwords must match. '
    }

    return data;
}
