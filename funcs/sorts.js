
// функция сортировки объекта в алфавитном порядке по его свойству
// arr - массив
// name - имя свойства
export const sortObjectCallbackByAlphabet = (arr = [], name = '') => {
    return arr.sort((a,b) => {
        let nameA = a['name'].toLowerCase(),
            nameB = b['name'].toLowerCase();

        if (nameA < nameB) {
            return -1
        }
        if (nameA > nameB) {
            return 1
        }
        return 0;
    })
};