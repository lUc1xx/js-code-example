/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

// require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import ReactDOM from "react-dom";
import React from "react";
import {Provider} from "react-redux";

// stores
import asideStore from './redux/aside/aside-store';

import AppBlade from './layouts/app.blade';
import Aside from './components/aside/aside';
import Footer from "./layouts/footer";
import Registration from "./components/forms/registration";
import Login from "./components/forms/login";
import PasswordEmail from "./components/forms/passwordEmail";
import PasswordReset from "./components/forms/passwordReset";

renderElement(<AppBlade/>, 'header');
renderReduxElement(<Aside/>, 'aside', asideStore);
renderElement(<Footer/>, 'footer');
renderElement(<Registration/>, 'registration');
renderElement(<Login/>, 'login');
renderElement(<PasswordEmail/>, 'passwordEmail');
renderElement(<PasswordReset/>, 'passwordReset');

function renderElement(com, wrapperId) {
    if (document.getElementById(wrapperId)) {
        ReactDOM.render(com, document.getElementById(wrapperId));
    }
}

function renderReduxElement(com, wrapperId, store) {
    if (document.getElementById(wrapperId)) {
        ReactDOM.render(
            <Provider store={store}>
                {com}
            </Provider>,
            document.getElementById(wrapperId)
        );
    }
}